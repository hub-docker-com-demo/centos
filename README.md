* [CentOS](https://en.wikipedia.org/wiki/CentOS)
* [yum](https://en.wikipedia.org/wiki/Yum_(software)) Now slowly replaced by dnf (see flatpak)
* [*Basic Yum Commands and how to use them*](http://yum.baseurl.org/wiki/YumCommands)
* [*How To Install Nginx on CentOS 7*](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-centos-7)

# For Flatpak see:
* https://gitlab.com/debian-packages-demo/flatpak

# End of life, replacements and comparisons
* [*CentOS 8 is dead: choosing a replacement Docker image*
  ](https://pythonspeed.com/articles/centos-8-is-dead/)
  2022-01 Itamar Turner-Trauring
* [*Comparing CentOS Stream and CentOS Linux: What's the difference between CentOS Stream and CentOS Linux?*
  ](https://www.centos.org/cl-vs-cs/)
  (2022) CentOS
